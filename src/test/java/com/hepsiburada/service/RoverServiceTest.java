package com.hepsiburada.service;
import com.hepsiburada.entity.Plateau;
import com.hepsiburada.entity.Position;
import com.hepsiburada.entity.Rover;
import com.hepsiburada.enums.DIRECTION;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class RoverServiceTest {

    @InjectMocks
    private RoverService roverService;

    private List<Rover> testRovers;

    @Before
    public void init(){
        testRovers=new ArrayList<Rover>();
        Plateau p= new Plateau(6,6);

        Rover r1=new Rover(new Position(1,2), DIRECTION.NORTH,1,p);
        testRovers.add(r1);

        Rover r2=new Rover(new Position(3,3), DIRECTION.EAST,2,p);
        testRovers.add(r2);
    }
    @Test
    public void testMove() throws Exception {
        roverService.move(testRovers.get(0),new String[]{"L","M","L","M","L","M","L","M","M"});
        assertEquals(testRovers.get(0).getPosition().getX(),1);
        assertEquals(testRovers.get(0).getPosition().getY(),3);
        assertEquals(testRovers.get(0).getDirection(),DIRECTION.NORTH);

        roverService.move(testRovers.get(1),new String[]{"M","M","R","M","M","R","M","R","R","M"});
        assertEquals(testRovers.get(1).getPosition().getX(),5);
        assertEquals(testRovers.get(1).getPosition().getY(),1);
        assertEquals(testRovers.get(1).getDirection(),DIRECTION.EAST);


    }
}
