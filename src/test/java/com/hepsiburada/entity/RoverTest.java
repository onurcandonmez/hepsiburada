package com.hepsiburada.entity;

import com.hepsiburada.enums.DIRECTION;
import com.hepsiburada.exception.InvalidMoveException;
import com.hepsiburada.service.RoverService;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.*;
@RunWith(MockitoJUnitRunner.class)
public class RoverTest {

    private Rover rover;
    private Rover movingRover;
    private Rover movingRover2;
    @Before
    public void setUp() throws Exception {
        Plateau p= new Plateau(6,6);
        rover=new Rover(new Position(1,2), DIRECTION.NORTH,1,p);

        movingRover=new Rover(new Position(0,0), DIRECTION.SOUTH,1,p);
        movingRover2=new Rover(new Position(5,5), DIRECTION.NORTH,1,p);
    }

    @Test
    public void turnLeft() {
        rover.turnLeft();
        assertEquals(rover.getDirection(),DIRECTION.WEST);
        rover.turnLeft();
        assertEquals(rover.getDirection(),DIRECTION.SOUTH);
        rover.turnLeft();
        assertEquals(rover.getDirection(),DIRECTION.EAST);
        rover.turnLeft();
        assertEquals(rover.getDirection(),DIRECTION.NORTH);
    }

    @Test
    public void turnRight() {
        rover.turnRight();
        assertEquals(rover.getDirection(),DIRECTION.EAST);
        rover.turnRight();
        assertEquals(rover.getDirection(),DIRECTION.SOUTH);
        rover.turnRight();
        assertEquals(rover.getDirection(),DIRECTION.WEST);
        rover.turnRight();
        assertEquals(rover.getDirection(),DIRECTION.NORTH);
    }

    @Test
    public void moveForward() {
        try {
            movingRover.moveForward();
        } catch (Exception e) {
           assertTrue(e instanceof InvalidMoveException);
        }
        try {
            movingRover.turnRight();
            movingRover.moveForward();
        } catch (Exception e) {
            assertTrue(e instanceof InvalidMoveException);
        }
        try {
            movingRover2.moveForward();
        } catch (Exception e) {
            assertTrue(e instanceof InvalidMoveException);
        }
        try {
            movingRover2.turnRight();
            movingRover2.moveForward();
        } catch (Exception e) {
            assertTrue(e instanceof InvalidMoveException);
        }
    }
}