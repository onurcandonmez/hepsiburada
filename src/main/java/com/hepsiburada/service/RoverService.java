package com.hepsiburada.service;

import java.util.ArrayList;

import com.hepsiburada.entity.Rover;
import com.hepsiburada.enums.DIRECTION;
import com.hepsiburada.enums.MOVE;

public class RoverService {

	public void move(Rover rover, String[] moveArr) throws Exception {
		for (int i = 0; i < moveArr.length; i++) {
			String m=moveArr[i];
			
			MOVE move=MOVE.getValueByLetter(m.charAt(0));
			 
			 if(move==MOVE.LEFT) {
				 rover.turnLeft();
				 
			 }else if(move==MOVE.RIGHT) {
				 rover.turnRight();
			 }else if(move==MOVE.MOVE) {
				 rover.moveForward();
			 }
			 
			 
		}
	}
	
	
}
