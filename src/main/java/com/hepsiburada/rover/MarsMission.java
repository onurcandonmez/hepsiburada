package com.hepsiburada.rover;

import java.util.Scanner;

import com.hepsiburada.entity.Plateau;
import com.hepsiburada.entity.Position;
import com.hepsiburada.entity.Rover;
import com.hepsiburada.enums.DIRECTION;
import com.hepsiburada.service.RoverService;

public class MarsMission {
	private Plateau plateau;
	
	public void start() {
		Scanner in = new Scanner(System.in);
		RoverService rs=new RoverService();
		
		String s = in.nextLine();
		
		String[] strs = s.trim().split("\\s+");
		int[] a=new int[strs.length];
		
		for (int i = 0; i < strs.length; i++) {
		    a[i] = Integer.parseInt(strs[i])+1;
		}
		plateau=new Plateau(a[0], a[1]);
		
		int roverCounter=0;
		
		while(true){
			
			String position = in.nextLine();
			
			if(position.equals("exit"))
				break;
			
			String[] positionArr = position.trim().split("\\s+");
			
			roverCounter++;
			
			int x=Integer.parseInt(positionArr[0]);
			int y =Integer.parseInt(positionArr[1]);
			
			Rover rover=new Rover(new Position(x,y),DIRECTION.getDirectionWithLetter(positionArr[2].charAt(0)), roverCounter,plateau);
			
			String move = in.nextLine();
			String[] moveArr=move.split("");
			
			try {
				rs.move(rover,moveArr);
				System.out.println(rover.toString());
			} catch (Exception e) {
				e.printStackTrace();
				System.err.println(e.getMessage());
			}
		}
		
	}
	
}
