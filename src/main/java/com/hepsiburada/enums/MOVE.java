package com.hepsiburada.enums;

public enum MOVE {
	LEFT('L',"Left"),RIGHT('W',"Right"),MOVE('M',"Move");
	
	private char movementLetter;
	private String movementName;
	
	MOVE(char movementLetter,String movementName){
		this.movementLetter=movementLetter;
		this.movementName=movementName;
	}
	
	public char getMovementLetter() {
		return movementLetter;
	}
	public String getMovementName() {
		return movementName;
	}

	public static MOVE getValueByLetter(char letter) {
		if(letter=='L')
			return MOVE.LEFT;
		else if(letter=='R')
			return MOVE.RIGHT;
		else if(letter=='M')
			return MOVE.MOVE;
		else return null;
	}
}
