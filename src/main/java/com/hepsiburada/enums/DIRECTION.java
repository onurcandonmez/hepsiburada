package com.hepsiburada.enums;

public enum DIRECTION {
	NORTH('N',"North"),WEST('W',"West"),EAST('E',"East"),SOUTH('S',"South");
	
	private char directionLetter;
	private String directionName;
	
	DIRECTION(char directionLetter,String directionName){
		this.directionLetter=directionLetter;
		this.directionName=directionName;
	}

	@Override
	public String toString() {

		return this.directionName;
	}

	public static DIRECTION getDirectionWithLetter(char charAt) {
		
		for(DIRECTION d:DIRECTION.values()) {
			if(d.directionLetter==charAt)
				return d;
		}
		return null;
	}
	public DIRECTION getLeftCardinalDirection() {
		if(this==DIRECTION.NORTH) return DIRECTION.WEST;
		else if(this==DIRECTION.WEST) return DIRECTION.SOUTH;
		else if(this==DIRECTION.SOUTH) return DIRECTION.EAST;
		else return DIRECTION.NORTH;
	}
	
	public DIRECTION getRightCardinalDirection() {
		if(this==DIRECTION.NORTH) return DIRECTION.EAST;
		else if(this==DIRECTION.EAST) return DIRECTION.SOUTH;
		else if(this==DIRECTION.SOUTH) return DIRECTION.WEST;
		else return DIRECTION.NORTH;
	}

	public char getDirectionLetter() {
		return directionLetter;
	}

	
}
