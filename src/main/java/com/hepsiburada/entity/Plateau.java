package com.hepsiburada.entity;

import java.util.ArrayList;
import java.util.List;

public class Plateau {
	private ArrayList<Rover>[][] area;
	
	public Plateau(int edgeA,int edgeB) {
		this.area= (ArrayList<Rover>[][])new ArrayList[edgeA][edgeB];
	}

	public ArrayList<Rover>[][] getArea() {
		return area;
	}


}
