package com.hepsiburada.entity;

import java.util.ArrayList;
import java.util.List;

import com.hepsiburada.enums.DIRECTION;
import com.hepsiburada.exception.InvalidMoveException;

public class Rover {
	
	private Position position;
	private DIRECTION direction;
	private Integer id;
	private Plateau plateau;
	
	
	public Rover(Position position, DIRECTION direction, Integer id, Plateau plateau) {
		this.position = position;
		this.direction = direction;
		this.id = id;
		this.plateau = plateau;
		
		ArrayList<Rover> rover=this.plateau.getArea()[position.getX()][position.getY()];
		
		if(rover==null) {
			ArrayList<Rover> roverList=new ArrayList<Rover>();
			roverList.add(this);
			this.plateau.getArea()[position.getX()][position.getY()]=roverList;
		}
			
	}
	
	public Position getPosition() {
		return position;
	}
	public DIRECTION getDirection() {
		return direction;
	}
	public Integer getId() {
		return id;
	}
	public Plateau getPlateau() {
		return plateau;
	}
	
	public void turnLeft() {
		this.direction=direction.getLeftCardinalDirection();
	}
	public void turnRight() {
		this.direction=direction.getRightCardinalDirection();
	}
	public void moveForward() throws Exception {
		
		if(direction.equals(DIRECTION.NORTH)){
			int newY=position.getY()+1;
			
			if(plateau.getArea().length<=newY)
				throw new InvalidMoveException("Out of boundry.");
			
			updatePosition(position.getX(),newY);
			
		}else if(direction.equals(DIRECTION.WEST)) {
			
			int newX=position.getX()-1;
			
			if(newX<0)
				throw new InvalidMoveException("Out of boundry.");
			
			updatePosition(newX,position.getY());
			
		}else if(direction.equals(DIRECTION.SOUTH)) {
			
			int newY=position.getY()-1;
			
			if(newY<0)
				throw new InvalidMoveException("Out of boundry.");
			
			updatePosition(position.getX(),newY);
			
		}else if(direction.equals(DIRECTION.EAST)) {
			
			int newX=position.getX()+1;
			
			if(newX>=plateau.getArea()[0].length)
				throw new InvalidMoveException("Out of boundry.");
			
			updatePosition(newX,position.getY());
			
		}
	}

	private void updatePosition(int x, int y) {
		List<Rover> roverList=plateau.getArea()[position.getX()][position.getY()];
		if(roverList!=null)
			roverList.remove(this);
		
		position.setX(x);
		position.setY(y);
		
		if(plateau.getArea()[position.getX()][position.getY()]!=null) {
			plateau.getArea()[position.getX()][position.getY()].add(this);
		}else{
			ArrayList<Rover> newRoverList=new ArrayList<Rover>();
			newRoverList.add(this);
			plateau.getArea()[position.getX()][position.getY()]=newRoverList;
		}
	}

	@Override
	public String toString() {
		StringBuffer sb=new StringBuffer();
		sb.append(position.getX());
		sb.append(" ");
		sb.append(position.getY());
		sb.append(" ");
		sb.append(direction.getDirectionLetter());
		return sb.toString();
	}
	
	
}
